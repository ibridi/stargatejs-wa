/*jshint esnext:true */

var expect = require("chai").expect;
var assert = require("chai").assert;
import {publicKey, privateKey} from '../keys/keys';
import {MessageResponse} from '../src/messageResponse';
import {Signer, Verifier} from '../src/crypter';
import {BasicSigner, BasicVerifier} from '../src/basicCrypter';

/** @test {Signer} */
describe('Test the Atlantis: Signer and MessageResponse', ()=>{

	it("Should work", ()=>{
		let s = new Signer(privateKey);
		var exec = "handshake",
			originalMsgId = "12qwe",
			success = "true",
			cbParams = {};

		var mp = new MessageResponse(exec, originalMsgId, success, cbParams);
		mp = s.sign(mp);

		let v = new Verifier(publicKey);		
		var result = v.verify(mp);
		expect(result).to.be.equal(true);

	});

	it("Should explode", ()=>{
		let s = new Signer(privateKey);
		var exec = "handshake",
			originalMsgId = "12qwe",
			success = "true",
			cbParams = {};

		var mp = new MessageResponse(exec, originalMsgId, success, cbParams);
		mp = s.sign(mp);

		let v = new Verifier(publicKey);	

		//change
		mp.success = false;	
		var result = v.verify(mp);
		expect(result).to.be.equal(false);

	});

	it("Verify with md5 should be true", ()=>{
		let s = new BasicSigner();

		var exec = "handshake",
		originalMsgId = "12qwe",
		success = "true",
		cbParams = {};

		var mp = new MessageResponse(exec, originalMsgId, success, cbParams);
		mp = s.sign(mp);

		let v = new BasicVerifier();

		var result = v.verify(mp);
		expect(result).to.be.equal(true);
	});
	
	it("Verify with md5 should be false", ()=>{
		let s = new BasicSigner();

		var exec = "handshake",
		originalMsgId = "12qwe",
		success = "true",
		cbParams = {};

		var mp = new MessageResponse(exec, originalMsgId, success, cbParams);
		mp = s.sign(mp);

		let v = new BasicVerifier();

		//change
		mp.success = false;
		mp.originalMsgId = "ewq21";
		var result = v.verify(mp);
		expect(result).to.be.equal(false);
	});
});