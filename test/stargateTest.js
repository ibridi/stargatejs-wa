/*jshint esnext:true */
var expect = require("chai").expect;
var assert = require("chai").assert;
var crypto = require("crypto-browserify");
var config = require('../config.json');

var MockBrowser = require('mock-browser').mocks.MockBrowser;
var window = MockBrowser.createWindow();
window.postMessage = function(){};

import {publicKey, privateKey} from '../keys/keys';
import {md5} from '../src/md5';
import {Message} from '../src/message';
import {send,read} from '../src/messenger';
import {singleton, singletonEnforcer, sendSymbol, crypterSymbol, handShakeSymbol} from '../src/constants';
import Stargate from '../src/stargate';
import LOG from '../src/logger';

/** @test {Stargate} */
describe('Test the Stargate Singleton', function(){

    var rightConfiguration;
    var wrongConfiguration;
    var callback;
    var onfail;
    before(function() {
        // runs before all tests in this block
        rightConfiguration = {
                "country":"it",
                "selector":"iphone",
                "api_selector":"zradio",
                "hybrid_conf":1 //more complex object
        };

        wrongConfiguration = {"key":2};

        callback = function(params){
            LOG.i("onSuccess", JSON.stringify(params));
        };
        
        onfail = function(params){
            
        }
    });

    beforeEach(function(){
        //
    });

    afterEach(function(){
        Stargate.destroyInstance();
    });

    /** @test {Stargate#initialize} */
    it('Test the two times initialization', function(){
        //First initialization
        Stargate.initialize(rightConfiguration, publicKey, callback, onfail, window);
        let inst = Stargate.getInstance();

        expect(inst.isInitialized()).to.be.equal(true);

        //Test the two times initialization: must return undefined
        var inst2;
        expect(function(){
            inst2 = Stargate.initialize(rightConfiguration, publicKey, callback, onfail, window)
        }).to.throw("Cannot initialize Stargate two times");
        expect(inst2).to.be.an('undefined');

    });

    it('Test the new Stargate() statement', function(){
        //Test the new Stargate() statement: must throw an error
        var sg;
        expect(function(){
            sg = new Stargate(Symbol(), rightConfiguration, "", function(){}, function(){}, window);

        }).to.throw("Cannot set up a singleton with new operator");
        expect(sg).to.be.an('undefined');
    });

    /** @test {Stargate#destroyInstance} */
    it('Test destroy instance', function() {

        //Test destroyInstance
        Stargate.initialize(rightConfiguration, publicKey, callback, onfail, window);
        Stargate.destroyInstance();
        var instance = Stargate.getInstance();
        expect(instance).to.be.an('undefined');

        Stargate.initialize({}, publicKey, callback, onfail, window);
        let inst3 = Stargate.getInstance();
        var obj = typeof Stargate.getInstance();
        expect(obj).to.be.equal("object");

    });

    it('Test ready fail ready', function() {
        Stargate.initialize(rightConfiguration, publicKey, callback, onfail, window);
        let Sg = Stargate.getInstance();
        //ready message
        expect(Sg.messages.size).to.be.equal(1);

    });

    /** @test {Stargate#getInstance} */
    it('Test getInstance before initialize', function() {
        let Sg = Stargate.getInstance();
        expect(Sg).to.be.an('undefined');
    });

    /** @test {Stargate#initialize} */
    it('Test the handshake with fake message ready. SignatureCheckError must be raised', function() {

        //initialize stargate

        var sg = Stargate.initialize(rightConfiguration, publicKey, callback, onfail, window);

        //prepare fake response to ready message without signature
        //there's only the ready message id value
        var readyID;
        for(let k of sg.messages.keys()){
            readyID = k;
        }

        var pm = {"exec":"handshake",
                  "originalMsgId":readyID,
                  "success":true,
                  "timestamp":Date.now(),
                  "signature":"this signature will fail"
        }

        var pMessage = {"origin":"file://", "data":JSON.stringify(pm)};
        expect(function(){
            read.call(sg, crypterSymbol, pMessage);
        }).to.throw("SignatureCheckError");

        //the message is still there because of signature check error
        expect(sg.messages.size).to.be.equal(1);
        expect(sg.isOpen()).to.be.equal(false);

    });

    /** @test {Stargate#initialize} */
    it('Test the handshake with fake message ready. Signature Check ok', function() {

        //initialize stargate
        var sg = Stargate.initialize(rightConfiguration, publicKey, callback, onfail, window);

        //prepare fake response to ready message without signature
        var readyID;
        for(let k of sg.messages.keys()){
            readyID = k;
        }

        var pm = {"exec":"handshake",
                  "originalMsgId":readyID,
                  "success":true,
                  "timestamp":Date.now(),
                  "signature":""
        }

        // signature atlantis
        var signer = crypto.createSign("RSA-SHA1");
        var text = pm.originalMsgId + pm.exec + pm.timestamp + (pm.success ? 'OK' : 'KO');
        signer.update(text);
        pm.signature = signer.sign(privateKey, "base64");
        // BasicCrypter signature
        pm.signature = md5(text);
        // signature atlantis


        var pMessage = {"origin":"file://", "data":JSON.stringify(pm)};

        expect(function(){
            read.call(sg, crypterSymbol, pMessage);
        }).to.not.throw("CrypterSymbolError");

        //the message must be erased
        expect(sg.messages.size).to.be.equal(0);
        expect(sg.isOpen()).to.be.equal(true);
    });

    /** @test {Stargate#initialize} */
    it('Test the handshake onfail fallback', (done)=> {
        var onFail = function(){
            console.log("[Stargate.js] onHandshakeFail");
            done();
        };

        var sg = Stargate.initialize(rightConfiguration, publicKey, callback, onFail, window);
        //handshake ready message if there's not response should be still there
        expect(sg.messages.size).to.be.equal(1);

        // and stargate should be opened
        expect(sg.isOpen()).to.be.equal(false);
    });

    /** @test {Stargate#read} */
    it('Atlantis Unexpected Request must be raised', ()=> {

        //initialize stargate

        var sg = Stargate.initialize(rightConfiguration, publicKey, callback, onfail, window);

        //Message Unexpected
        var pm = {"exec":"handshake",
                  "originalMsgId":"123",
                  "success":true,
                  "timestamp":Date.now(),
                  "signature":""
        }

        var pMessage = {"origin":"file://", "data":JSON.stringify(pm)};
        expect(function(){
            read.call(sg, crypterSymbol, pMessage);
        }).to.throw("Atlantis Request Unexpected");

        //the message is still there because of Error thrown
        expect(sg.messages.size).to.be.equal(1);
        expect(sg.isOpen()).to.be.equal(false);

    });
});
