/*jshint esnext:true */
var expect = require("chai").expect;
var crypto = require('crypto-browserify');
import {Verifier} from '../src/crypter';
import {publicKey, privateKey} from '../keys/keys';
import {Message} from '../src/message';

/** @test {Crypter} */
describe('Test the Crypter', function(){

    /** @test {Crypter#verify} */
    it('test the verify functionality with node-crypto', function() {

    	var myCrypter = new Verifier(publicKey);    	
    	var m = new Message('ready');    	

    	// ----- like atlantis will do: TODO: testUtils.js
        var signer = crypto.createSign("RSA-SHA1");
    	m.timestamp = Date.now();
    	var text = m.originalMsgId + m.exec + m.timestamp + (m.success ? 'OK' : 'KO');
    	signer.update(text);
    	m.signature = signer.sign(privateKey, "base64");
    	//-----

    	var res = myCrypter.verify(m);
    	expect(true).to.equal(res);
    	
    });
});