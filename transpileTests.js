#!/usr/bin/env node
var path = require("path");
var fs = require("fs");
var browserify = require("browserify");
var babelify = require("babelify");

function transpileTests(dir){
	/*
	* Take every *.js in test/ folder 
	* and transpile/browserify it in test5/ folder
	*/

	var patternForTestFiles = new RegExp(".js$");
	
	fs.readdir(dir, function(err, list){		
		if (err) {
	        throw err;
	    }

	    list.map(function(filepath){
	    	if(patternForTestFiles.test(filepath)){
	    		
	    		var dirpath = path.join(dir, filepath);
	    		var test5path = path.join("test5", filepath);
	    		console.log("Transpiling test... " + dirpath + " to " + test5path);

				browserify({ debug: true })
				.transform(babelify)
				.require(dirpath, { entry: true })
				.bundle()
				.on("error", function (err) { console.log("Error: " + err.message); })
				.pipe(fs.createWriteStream(test5path));

	    	}
	    });
	});

}

transpileTests("test");