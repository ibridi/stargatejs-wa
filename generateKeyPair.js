var fs = require('fs');
var shelljs = require('shelljs');
var exec = require('child_process').exec,
    child;

//generate PEM Keys
shelljs.exec('openssl genrsa -out keys/rsa_1024_priv.pem 1024');
shelljs.exec('openssl rsa -in keys/rsa_1024_priv.pem -out keys/rsa_1024_pub.pem -outform PEM -pubout');
var fullRegex = /^(-----BEGIN(.*)KEY-----)\n([0-9A-z\n\r\+\/\=]+)\n(-----END\2KEY-----)/;

//Read PEM files
var privateString = fs.readFileSync('keys/rsa_1024_priv.pem', 'utf8');
var publicString = fs.readFileSync('keys/rsa_1024_pub.pem', 'utf8');

//Remove \n\r inside the keys but let it after BEGIN and before END
//Crypto browserify has a little bug when a key with has not \n
var matched = privateString.match(fullRegex);
var matchedPub = publicString.match(fullRegex);
var privateKeyString = matched[1]+"\n"+matched[3]+"\n"+matched[4];
var publicKeyString = matchedPub[1]+"\n"+matchedPub[3]+"\n"+matchedPub[4];

fs.writeFile("keys/keys.js", "export let privateKey = `" + privateKeyString + "`;\n\nexport let publicKey = `" + publicKeyString + "`;", function(err){
  if (err) return console.log(err);
});
//openssl genrsa -out rsa_1024_priv.pem 1024
//openssl rsa -in rsa_1024_priv.pem -out rsa_1024_pub.pem -outform PEM -pubout