/*jshint esnext:true */
/** 
* This Symbol variable it's used to enforce Stargate singleton behaviour 
* @param {Symbol} singleton - Use for stargate behaviour
*/

let possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

function generateUnique(){
	var text = "";
	for(var i = 0; i < 10; i++){
	  text += possible.charAt(Math.floor(Math.random() * possible.length));
	}
	return text;
}

export var singleton = generateUnique();

/** 
* This Symbol variable it's used to enforce Stargate singleton behaviour 
* @param {Symbol} singleton - Use for stargate behaviour
*/
export var singletonEnforcer = generateUnique();

/** 
* This Symbol variable it's used to have private members in stargate
* @param {Symbol} sendSymbol - sendSymbol description
*/
export const sendSymbol = generateUnique();

/** 
* This Symbol variable it's used to have private members in stargate
* @param {Symbol} crypterSymbol - crypterSymbol description
*/
export const crypterSymbol = generateUnique();

/** 
* This Symbol variable it's used to have private members in stargate
* @param {Symbol} handShakeSymbol - handShakeSymbol description
*/
export const handShakeSymbol = generateUnique();