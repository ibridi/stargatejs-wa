/**
  * The AbstractCrypter class is a simple class interface skeleton with just one method
  * @property {string} algo - The type of sign/verify algorithm example RSA-SHA1
  */
export class AbstractCrypter{

  constructor(){
    this.algo = "RSA-SHA1";
  }
  /** GenerateUnique the function
  * @param {Message} message - The message from whom generate the unique checksum to sign
  * @return {string} unique - the checksum to sign
  */
  _generateUnique(message){
    return message.originalMsgId + message.exec + message.timestamp + (message.success ? 'OK' : 'KO');
  }
}
