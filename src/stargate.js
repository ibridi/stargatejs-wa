/*jshint esnext:true */
import {Message} from './message';
//import {Verifier} from './crypter';
import {BasicVerifier} from './basicCrypter';
import {send,read} from './messenger';
import {singleton, singletonEnforcer, sendSymbol, crypterSymbol, handShakeSymbol} from './constants';
import LOG from './logger';
var _singleton = singleton;
var _singletonEnforcer = singletonEnforcer;
var privates = new WeakMap();

LOG.setLevel(4); //0 - 4
var TAG = "[Stargate.js]";
/**
 * Stargate is a singleton class for hybridation of B! iframed web app
 *
 * @example
 * var instance = Stargate.initialize(configurations, publicKey, onHandshake, onHandshakeFail);
 * Stargate.getInstance();
 */
export default class Stargate{

    constructor(enforcer, configurations, publicKey, onHandshake, onHandshakeFail, global=window) {
        if(enforcer != singletonEnforcer) throw new Error("Cannot set up a singleton with new operator");

        /** @type {Object}
          * @property {string} configurations.country - example it
          * @property {string} configurations.selector - example iphone
          * @property {string} configurations.api_selector - example zradio
          * @property {Object} configurations.hybrid_conf - the hybrid conf object
          */
        this.configurations = configurations;

        /** @type {Symbol} */
        this[handShakeSymbol] = onHandshake;

        /** @type {Symbol} */
        //this[crypterSymbol] = new Verifier(publicKey);
        this[crypterSymbol] = new BasicVerifier();

        /** @type {boolean} */
        this.open = false;

        /** @type {boolean} */
        this.initialized = true;

        /** @type {Map<string, Message>} */
        this.messages = new Map();

        /** @type {Window} */
        this.global = global;

        let readyMessage = new Message('ready', onHandshake, onHandshakeFail, {"key":"value"});
        readyMessage.configuration = this.configurations;

        this.messages.set(readyMessage.msgId, readyMessage);

        /** @type {WeakMap} */
        privates.set(this, {
            _send:send,
            _read:read
        });

        //Send the ready message
        privates.get(this)._send.call(this, readyMessage);

        //if after 200ms i don't have an answer call the callback error        
        setTimeout(()=>{
            //if no response after 200ms means no one answer on hybrid
            if(this.messages.has(readyMessage.msgId)){
                if(onHandshakeFail){
                    onHandshakeFail();    
                }                
            } 
        }, 200);

        //listener will be called with Stargate and crypterSymbol
        var listener = privates.get(this)._read.bind(this, crypterSymbol);

        //Attach listener for postMessage
        this.global.addEventListener('message', listener, false);
    }

    /**
     * Build and returns a new Stargate instance.
     *
     * @param {Object} configurations - The configurations object
     * @param {String} publicKey - the publickey for the crypter object
     * @param {Function} onHandshake - the ready callback function
     * @param {Function} onHandshakeFail - the fail callback function. after 200ms is fired if no handshake happens.
     * @param {Window} [global=window] - the global scope of the SDK
     * @return {Stargate} the stargate instance
     */
    static initialize(configurations, publicKey, onHandshake=function(){}, onHandshakeFail=function(){}, global=window) {

        if(!this[_singleton]) {
          this[_singleton] = new Stargate(singletonEnforcer, configurations, publicKey, onHandshake, onHandshakeFail, global);
        }
        else{
            throw new Error("Cannot initialize Stargate two times");
        }
        return this[_singleton];
    }

    /**
    * Getter for the instance of the initialized stargate. if it is called before returns undefined
    * @return {Stargate} the stargate instance
    */
    static getInstance(){
        if(!this[_singleton]){
            LOG.w(TAG, "Please call initialize first: Stargate.initialize(conf, pubkey, callback)");
        }
        return this[_singleton];
    }

    /**
    * Destroy the current instance of the stargate setting to null the internal reference.
    */
    static destroyInstance(){
        this[_singleton] = null;
        _singleton = Symbol();
        _singletonEnforcer = Symbol();
    }

    /**
    * Purchase.
    * @param {string} productId - The id of the product to purchase
    * @param {Function} callbackSuccess - the success callback function on response
    * @param {Function} callbackError - the callbackError function on response
    * @param {string} urlForCreateUser - the url of the serverside script to create the user on our server
    */
    inAppPurchase(productId, callbackSuccess, callbackError, createUserUrl=""){
        let message = new Message("stargate.purchase", callbackSuccess, callbackError);
        message.createUserUrl = createUserUrl;

        this.messages.set(message.msgId, message);
        privates.get(this)._send.call(this, message);
    }

    /**
    * Purchase subscription.
    * @param {Function} callbackSuccess - the callbakc success
    * @param {Function} callbackError - the callback error
    * @param {String} subscriptionUrl - the subscriptionUrl
    * @param {String} returnUrl - the returnedUrl
    */
    inAppPurchaseSubscription(callbackSuccess, callbackError, subscriptionUrl="", returnUrl=""){
        let message = new Message("stargate.purchase.subscription", callbackSuccess, callbackError);
        message.subscriptionUrl = subscriptionUrl;
        message.returnUrl = returnUrl;

        this.messages.set(message.msgId, message);
        privates.get(this)._send.call(this, message);
    }

    /**
    * Restore a previous purchase.
    * @param {Function} callbackSuccess - the success callback function on response
    * @param {Function} callbackError - the callbackError function on response
    * @param {string} subscriptionUrl - the url of the serverside script to create the user on our server
    * @param {string} returnUrl - returnUrl
    */
    inAppRestore(callbackSuccess, callbackError, subscriptionUrl="", returnUrl=""){
        let message = new Message("stargate.restore", callbackSuccess, callbackError);
        message.subscriptionUrl = subscriptionUrl;
        message.returnUrl = returnUrl;

        this.messages.set(message.msgId, message);
        privates.get(this)._send.call(this, message);
    }

    /**
    * Native Facebook login.
    * @param {string} scope - The permissions to ask
    * @param {Function} callbackSuccess - the callback success
    * @param {Function} callbackError - the callback error
    */
    facebookLogin(scope, callbackSuccess, callbackError){
        let message = new Message("stargate.facebookLogin", callbackSuccess, callbackError);

        message.scope = scope;
        this.messages.set(message.msgId, message);
        privates.get(this)._send.call(this, message);
    }

    /**
    * Native Google login.
    * @param {Function} callbackSuccess - the callback success
    * @param {Function} callbackError - the callback error
    */
    googleLogin(callbackSuccess, callbackError){
        let message = new Message("stargate.googleLogin", callbackSuccess, callbackError);

        this.messages.set(message.msgId, message);
        privates.get(this)._send.call(this, message);
    }

    /**
    * It will open the url given in a browser device tab outside the web app.
    * @param {string} url - the url to open in a different window on the device
    */
    openUrl(url){
        let message = new Message("system");
        message.url = url;

        this.messages.set(message.msgId, message);
        privates.get(this)._send.call(this, message);
    }

    /**
    * The inframe function description.
    * @return {boolean} Return if Stargate is running or not in iframe
    */
    inIframe(){
      return this.global.self != this.global.parent;
    }

    /**
    * isOpen function
    * @return {boolean} Return true if the handshake happened
    */
    isOpen(){
      return this.open;
    }

    /**
    * isInitialized function
    * @return {boolean} Return true if the initialize method was called
    */
    isInitialized(){
        return this.initialized;
    }

    execFunctionOnApp(funcName, funcArgs){
      let message = new Message(funcName);
      message.data = {"args":funcArgs};

      this.messages.set(message.msgId, message);
      privates.get(this)._send.call(this, message);
    }
}