/*jshint esnext:true */
import crypto from 'crypto-browserify';
import {AbstractCrypter} from './abstractCrypter';

/** 
  * The Verifier class is a simple class to verify a Message
  * @property {string} pubkey - The publicKey PEM formatted string
  * @param {function} verifier - The verifier crypto function
  */
export class Verifier extends AbstractCrypter{

    /** Verifier constructor is based on crypto-browserify npm package
      * @param {string} pubkey - Is a publicKey PEM formatted string
      */
    constructor(pubkey){
        super();
        try{
            /** @type {string} */
            this.pubkey = pubkey;            

            /** @see https://nodejs.org/api/crypto.html#crypto_class_verify */
            this.verifier = crypto.createVerify(this.algo);
        }catch(e){
            console.log(e);
        }
    }

    /** Verify the message signature and the checksum.      
      * @param {Message} message - The message from whom generates the unique checksum to sign
      * @return {boolean} verification result
      */
    verify(message){
        this.verifier.update(this._generateUnique(message).toString());     
        let result = this.verifier.verify(this.pubkey, message.signature, 'base64');
        return result;
    }
}

/** 
  * The Signer class is a simple class to sign a Message
  * @property {string} privkey - The privateKey PEM formatted string
  * @param {function} signer - The signer crypto function
  */
export class Signer extends AbstractCrypter{

  constructor(privkey){
    super();
    this.signer = crypto.createSign(this.algo);
    this.privkey = privkey;
  }

  /** Sign the message with signature's checksum.      
  * @param {Message} message - The message from whom generate the unique checksum that will be signed
  * @return {Message} the Message signed
  */
  sign(messageResponse){      
      this.signer.update(this._generateUnique(messageResponse));
      messageResponse.signature = this.signer.sign(this.privkey, "base64");
      return messageResponse;
  }
}