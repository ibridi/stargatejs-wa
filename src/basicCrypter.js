/*jshint esnext:true */
import {AbstractCrypter} from './abstractCrypter';
import {md5} from './md5';

export class BasicSigner extends AbstractCrypter{

	constructor(){
		super();
		this.algo = "md5";
	}

	sign(messageResponse){
		let signature = md5(this._generateUnique(messageResponse));
		messageResponse.signature = signature;
		return messageResponse;
	}
}

export class BasicVerifier extends AbstractCrypter{

	constructor(){
		super();
		this.algo = "md5";
	}

	verify(message){
		return message.signature === md5(this._generateUnique(message));
	}
}