export default {
  level:0,
  e:function(){
    if(this.level != 0 && this.level >= 4){
        console.error.apply(console, arguments);
    }
  },
  i:function(){
    if(this.level != 0 && this.level >= 3){
      console.info.apply(console, arguments);
    }
  },
  w:function(){
    if(this.level != 0 && this.level >= 2){
      console.warn.apply(console, arguments);
    }
  },
  d:function(){
    if(this.level != 0 && this.level >= 1){
      console.log.apply(console, arguments);
    }
  },
  setLevel:function(level){
    this.level = level;
  }
};
