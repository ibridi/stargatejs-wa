/*jshint esnext:true */
export class MessageResponse{
	constructor(exec, originalMsgId, success, cbParams){
		this.exec = exec;
		this.originalMsgId = originalMsgId;
		this.success = success;
		this.cbParams = cbParams;
		this.signature = "";
		this.timestamp = Date.now();
	}
}