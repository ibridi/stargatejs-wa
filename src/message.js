/*jshint esnext:true */
/**
* A simple message class with self generated id
* @example
* var message = new Message(commandToExecute, successCallback, errorCallback, callbackParams);
* console.log(message.msgId);
*/
export class Message{
  /**
  * Message Constructor
  * @param {string} exec - The command to execute
  * @param {function} callbackSuccess - the success callback to execute on app response
  * @param {function} callbackError - the error callback to execute on app response
  * @param {Object} cbparams - a JSON object to call with callbacks
  */
  constructor(exec, callbackSuccess=function(data){console.log(data)}, 
                    callbackError=function(data){console.log(data)}, 
                    cbparams={}){
    
    /** @type {boolean}*/
    this.exec = exec || false;

    this.error = false;
    
    /** @type {function}*/
    this.callbackSuccess = callbackSuccess;

    /** @type {function}*/
    this.callbackError = callbackError;

    /** @type {Object}*/
    this.callbackParams = cbparams;

    /** @type {string}*/
    this.msgId = this._createMessageId();
  }

  /**
  * CreateMessageId function
  * @access private
  * @return {string} id - unique id for the message
  */
  _createMessageId(){
    let text = "";
    let possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    for(var i = 0; i < 5; i++){
      text += possible.charAt(Math.floor(Math.random() * possible.length));
    }
    return text;
  }
}