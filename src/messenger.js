/*jshint esnext:true */
/**
* Stargate send private function
* @param {Message} message - The Message to send
*/
import LOG from './logger';
var TAG = "[Stargate.js]";

export function send(message){
	this.global.parent.postMessage(JSON.stringify(message), '*');
}

/**
* Stargate read private function
* @param {MessageEvent} event - The MessageEvent posteMessaged from native layer (https://developer.mozilla.org/en-US/docs/Web/API/MessageEvent)
* @param {Symbol} crypterSymbol - The symbol to privately call the crypter in Stargate
*/
export function read(crypterSymbol, event){
		var msg;
		LOG.d(TAG, "Message Received");

		if (event.origin !== "file://"){
				LOG.w(TAG, "A request from: "+event.origin+ " skipped...");
				return;
		}

		let data = event.data;
		if(data){
			try {
				var parsedData = JSON.parse(data);
				msg = this.messages.get(parsedData.originalMsgId);
			}catch (e) {
				LOG.e(TAG, e);
				return;
			}
		}

		if (msg === undefined) {
			throw new Error("Atlantis Request Unexpected");
			return;
		}

		for(var key in parsedData){
          if (typeof parsedData[key] !== 'function') {
            msg[key] = parsedData[key];
          }
    }

	if (typeof msg['success'] !== "undefined"){
		if(!this[crypterSymbol]){ throw new Error("CrypterSymbolError");}
		let verified = this[crypterSymbol].verify(msg);

		if(verified){
			if(parsedData["exec"] == "handshake"){
				this.open = true;
			}

			if(msg['success']){
				this.messages.get(msg.originalMsgId).callbackSuccess(msg.callbackParams);
			}else{
				this.messages.get(msg.originalMsgId).callbackError(msg.callbackParams);
			}
		}else{
			throw new Error("SignatureCheckError");
			return;
		}
  	this.messages.delete(msg.originalMsgId);
	}
}
