(function(f){if(typeof exports==="object"&&typeof module!=="undefined"){module.exports=f()}else if(typeof define==="function"&&define.amd){define([],f)}else{var g;if(typeof window!=="undefined"){g=window}else if(typeof global!=="undefined"){g=global}else if(typeof self!=="undefined"){g=self}else{g=this}g.Stargate = f()}})(function(){var define,module,exports;return (function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, '__esModule', {
  value: true
});

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }

/**
  * The AbstractCrypter class is a simple class interface skeleton with just one method
  * @property {string} algo - The type of sign/verify algorithm example RSA-SHA1
  */

var AbstractCrypter = (function () {
  function AbstractCrypter() {
    _classCallCheck(this, AbstractCrypter);

    this.algo = 'RSA-SHA1';
  }

  _createClass(AbstractCrypter, [{
    key: '_generateUnique',

    /** GenerateUnique the function
    * @param {Message} message - The message from whom generate the unique checksum to sign
    * @return {string} unique - the checksum to sign
    */
    value: function _generateUnique(message) {
      return message.originalMsgId + message.exec + message.timestamp + (message.success ? 'OK' : 'KO');
    }
  }]);

  return AbstractCrypter;
})();

exports.AbstractCrypter = AbstractCrypter;

},{}],2:[function(require,module,exports){
/*jshint esnext:true */
'use strict';

Object.defineProperty(exports, '__esModule', {
	value: true
});

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

var _get = function get(_x, _x2, _x3) { var _again = true; _function: while (_again) { var object = _x, property = _x2, receiver = _x3; desc = parent = getter = undefined; _again = false; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { _x = parent; _x2 = property; _x3 = receiver; _again = true; continue _function; } } else if ('value' in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } } };

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }

function _inherits(subClass, superClass) { if (typeof superClass !== 'function' && superClass !== null) { throw new TypeError('Super expression must either be null or a function, not ' + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) subClass.__proto__ = superClass; }

var _abstractCrypter = require('./abstractCrypter');

var _md5 = require('./md5');

var BasicSigner = (function (_AbstractCrypter) {
	function BasicSigner() {
		_classCallCheck(this, BasicSigner);

		_get(Object.getPrototypeOf(BasicSigner.prototype), 'constructor', this).call(this);
		this.algo = 'md5';
	}

	_inherits(BasicSigner, _AbstractCrypter);

	_createClass(BasicSigner, [{
		key: 'sign',
		value: function sign(messageResponse) {
			var signature = (0, _md5.md5)(this._generateUnique(messageResponse));
			messageResponse.signature = signature;
			return messageResponse;
		}
	}]);

	return BasicSigner;
})(_abstractCrypter.AbstractCrypter);

exports.BasicSigner = BasicSigner;

var BasicVerifier = (function (_AbstractCrypter2) {
	function BasicVerifier() {
		_classCallCheck(this, BasicVerifier);

		_get(Object.getPrototypeOf(BasicVerifier.prototype), 'constructor', this).call(this);
		this.algo = 'md5';
	}

	_inherits(BasicVerifier, _AbstractCrypter2);

	_createClass(BasicVerifier, [{
		key: 'verify',
		value: function verify(message) {
			return message.signature === (0, _md5.md5)(this._generateUnique(message));
		}
	}]);

	return BasicVerifier;
})(_abstractCrypter.AbstractCrypter);

exports.BasicVerifier = BasicVerifier;

},{"./abstractCrypter":1,"./md5":5}],3:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
	value: true
});
/*jshint esnext:true */
/** 
* This Symbol variable it's used to enforce Stargate singleton behaviour 
* @param {Symbol} singleton - Use for stargate behaviour
*/

var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

function generateUnique() {
	var text = "";
	for (var i = 0; i < 10; i++) {
		text += possible.charAt(Math.floor(Math.random() * possible.length));
	}
	return text;
}

var singleton = generateUnique();

exports.singleton = singleton;
/** 
* This Symbol variable it's used to enforce Stargate singleton behaviour 
* @param {Symbol} singleton - Use for stargate behaviour
*/
var singletonEnforcer = generateUnique();

exports.singletonEnforcer = singletonEnforcer;
/** 
* This Symbol variable it's used to have private members in stargate
* @param {Symbol} sendSymbol - sendSymbol description
*/
var sendSymbol = generateUnique();

exports.sendSymbol = sendSymbol;
/** 
* This Symbol variable it's used to have private members in stargate
* @param {Symbol} crypterSymbol - crypterSymbol description
*/
var crypterSymbol = generateUnique();

exports.crypterSymbol = crypterSymbol;
/** 
* This Symbol variable it's used to have private members in stargate
* @param {Symbol} handShakeSymbol - handShakeSymbol description
*/
var handShakeSymbol = generateUnique();
exports.handShakeSymbol = handShakeSymbol;

},{}],4:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = {
  level: 0,
  e: function e() {
    if (this.level != 0 && this.level >= 4) {
      console.error.apply(console, arguments);
    }
  },
  i: function i() {
    if (this.level != 0 && this.level >= 3) {
      console.info.apply(console, arguments);
    }
  },
  w: function w() {
    if (this.level != 0 && this.level >= 2) {
      console.warn.apply(console, arguments);
    }
  },
  d: function d() {
    if (this.level != 0 && this.level >= 1) {
      console.log.apply(console, arguments);
    }
  },
  setLevel: function setLevel(level) {
    this.level = level;
  }
};
module.exports = exports["default"];

},{}],5:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
var md5 = (function () {
  function e(e, t) {
    var o = e[0],
        u = e[1],
        a = e[2],
        f = e[3];o = n(o, u, a, f, t[0], 7, -680876936);f = n(f, o, u, a, t[1], 12, -389564586);a = n(a, f, o, u, t[2], 17, 606105819);u = n(u, a, f, o, t[3], 22, -1044525330);o = n(o, u, a, f, t[4], 7, -176418897);f = n(f, o, u, a, t[5], 12, 1200080426);a = n(a, f, o, u, t[6], 17, -1473231341);u = n(u, a, f, o, t[7], 22, -45705983);o = n(o, u, a, f, t[8], 7, 1770035416);f = n(f, o, u, a, t[9], 12, -1958414417);a = n(a, f, o, u, t[10], 17, -42063);u = n(u, a, f, o, t[11], 22, -1990404162);o = n(o, u, a, f, t[12], 7, 1804603682);f = n(f, o, u, a, t[13], 12, -40341101);a = n(a, f, o, u, t[14], 17, -1502002290);u = n(u, a, f, o, t[15], 22, 1236535329);o = r(o, u, a, f, t[1], 5, -165796510);f = r(f, o, u, a, t[6], 9, -1069501632);a = r(a, f, o, u, t[11], 14, 643717713);u = r(u, a, f, o, t[0], 20, -373897302);o = r(o, u, a, f, t[5], 5, -701558691);f = r(f, o, u, a, t[10], 9, 38016083);a = r(a, f, o, u, t[15], 14, -660478335);u = r(u, a, f, o, t[4], 20, -405537848);o = r(o, u, a, f, t[9], 5, 568446438);f = r(f, o, u, a, t[14], 9, -1019803690);a = r(a, f, o, u, t[3], 14, -187363961);u = r(u, a, f, o, t[8], 20, 1163531501);o = r(o, u, a, f, t[13], 5, -1444681467);f = r(f, o, u, a, t[2], 9, -51403784);a = r(a, f, o, u, t[7], 14, 1735328473);u = r(u, a, f, o, t[12], 20, -1926607734);o = i(o, u, a, f, t[5], 4, -378558);f = i(f, o, u, a, t[8], 11, -2022574463);a = i(a, f, o, u, t[11], 16, 1839030562);u = i(u, a, f, o, t[14], 23, -35309556);o = i(o, u, a, f, t[1], 4, -1530992060);f = i(f, o, u, a, t[4], 11, 1272893353);a = i(a, f, o, u, t[7], 16, -155497632);u = i(u, a, f, o, t[10], 23, -1094730640);o = i(o, u, a, f, t[13], 4, 681279174);f = i(f, o, u, a, t[0], 11, -358537222);a = i(a, f, o, u, t[3], 16, -722521979);u = i(u, a, f, o, t[6], 23, 76029189);o = i(o, u, a, f, t[9], 4, -640364487);f = i(f, o, u, a, t[12], 11, -421815835);a = i(a, f, o, u, t[15], 16, 530742520);u = i(u, a, f, o, t[2], 23, -995338651);o = s(o, u, a, f, t[0], 6, -198630844);f = s(f, o, u, a, t[7], 10, 1126891415);a = s(a, f, o, u, t[14], 15, -1416354905);u = s(u, a, f, o, t[5], 21, -57434055);o = s(o, u, a, f, t[12], 6, 1700485571);f = s(f, o, u, a, t[3], 10, -1894986606);a = s(a, f, o, u, t[10], 15, -1051523);u = s(u, a, f, o, t[1], 21, -2054922799);o = s(o, u, a, f, t[8], 6, 1873313359);f = s(f, o, u, a, t[15], 10, -30611744);a = s(a, f, o, u, t[6], 15, -1560198380);u = s(u, a, f, o, t[13], 21, 1309151649);o = s(o, u, a, f, t[4], 6, -145523070);f = s(f, o, u, a, t[11], 10, -1120210379);a = s(a, f, o, u, t[2], 15, 718787259);u = s(u, a, f, o, t[9], 21, -343485551);e[0] = m(o, e[0]);e[1] = m(u, e[1]);e[2] = m(a, e[2]);e[3] = m(f, e[3]);
  }
  function t(e, t, n, r, i, s) {
    t = m(m(t, e), m(r, s));return m(t << i | t >>> 32 - i, n);
  }function n(e, n, r, i, s, o, u) {
    return t(n & r | ~n & i, e, n, s, o, u);
  }
  function r(e, n, r, i, s, o, u) {
    return t(n & i | r & ~i, e, n, s, o, u);
  }function i(e, n, r, i, s, o, u) {
    return t(n ^ r ^ i, e, n, s, o, u);
  }
  function s(e, n, r, i, s, o, u) {
    return t(r ^ (n | ~i), e, n, s, o, u);
  }function o(t) {
    var n = t.length,
        r = [1732584193, -271733879, -1732584194, 271733878],
        i;
    for (i = 64; i <= t.length; i += 64) {
      e(r, u(t.substring(i - 64, i)));
    }t = t.substring(i - 64);var s = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
    for (i = 0; i < t.length; i++) s[i >> 2] |= t.charCodeAt(i) << (i % 4 << 3);s[i >> 2] |= 128 << (i % 4 << 3);if (i > 55) {
      e(r, s);for (i = 0; i < 16; i++) s[i] = 0;
    }s[14] = n * 8;e(r, s);return r;
  }
  function u(e) {
    var t = [],
        n;for (n = 0; n < 64; n += 4) {
      t[n >> 2] = e.charCodeAt(n) + (e.charCodeAt(n + 1) << 8) + (e.charCodeAt(n + 2) << 16) + (e.charCodeAt(n + 3) << 24);
    }return t;
  }
  function c(e) {
    var t = "",
        n = 0;for (; n < 4; n++) t += a[e >> n * 8 + 4 & 15] + a[e >> n * 8 & 15];return t;
  }
  function h(e) {
    for (var t = 0; t < e.length; t++) e[t] = c(e[t]);return e.join("");
  }
  function d(e) {
    return h(o(unescape(encodeURIComponent(e))));
  }
  function m(e, t) {
    return e + t & 4294967295;
  }var a = "0123456789abcdef".split("");return d;
})();
exports.md5 = md5;

},{}],6:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

/*jshint esnext:true */
/**
* A simple message class with self generated id
* @example
* var message = new Message(commandToExecute, successCallback, errorCallback, callbackParams);
* console.log(message.msgId);
*/

var Message = (function () {
  /**
  * Message Constructor
  * @param {string} exec - The command to execute
  * @param {function} callbackSuccess - the success callback to execute on app response
  * @param {function} callbackError - the error callback to execute on app response
  * @param {Object} cbparams - a JSON object to call with callbacks
  */

  function Message(exec) {
    var callbackSuccess = arguments[1] === undefined ? function (data) {
      console.log(data);
    } : arguments[1];
    var callbackError = arguments[2] === undefined ? function (data) {
      console.log(data);
    } : arguments[2];
    var cbparams = arguments[3] === undefined ? {} : arguments[3];

    _classCallCheck(this, Message);

    /** @type {boolean}*/
    this.exec = exec || false;

    this.error = false;

    /** @type {function}*/
    this.callbackSuccess = callbackSuccess;

    /** @type {function}*/
    this.callbackError = callbackError;

    /** @type {Object}*/
    this.callbackParams = cbparams;

    /** @type {string}*/
    this.msgId = this._createMessageId();
  }

  _createClass(Message, [{
    key: "_createMessageId",

    /**
    * CreateMessageId function
    * @access private
    * @return {string} id - unique id for the message
    */
    value: function _createMessageId() {
      var text = "";
      var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

      for (var i = 0; i < 5; i++) {
        text += possible.charAt(Math.floor(Math.random() * possible.length));
      }
      return text;
    }
  }]);

  return Message;
})();

exports.Message = Message;

},{}],7:[function(require,module,exports){
/*jshint esnext:true */
/**
* Stargate send private function
* @param {Message} message - The Message to send
*/
'use strict';

Object.defineProperty(exports, '__esModule', {
	value: true
});
exports.send = send;

/**
* Stargate read private function
* @param {MessageEvent} event - The MessageEvent posteMessaged from native layer (https://developer.mozilla.org/en-US/docs/Web/API/MessageEvent)
* @param {Symbol} crypterSymbol - The symbol to privately call the crypter in Stargate
*/
exports.read = read;

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }

var _logger = require('./logger');

var _logger2 = _interopRequireDefault(_logger);

var TAG = '[Stargate.js]';

function send(message) {
	this.global.parent.postMessage(JSON.stringify(message), '*');
}

function read(crypterSymbol, event) {
	var msg;
	_logger2['default'].d(TAG, 'Message Received');

	if (event.origin !== 'file://') {
		_logger2['default'].w(TAG, 'A request from: ' + event.origin + ' skipped...');
		return;
	}

	var data = event.data;
	if (data) {
		try {
			var parsedData = JSON.parse(data);
			msg = this.messages.get(parsedData.originalMsgId);
		} catch (e) {
			_logger2['default'].e(TAG, e);
			return;
		}
	}

	if (msg === undefined) {
		throw new Error('Atlantis Request Unexpected');
		return;
	}

	for (var key in parsedData) {
		if (typeof parsedData[key] !== 'function') {
			msg[key] = parsedData[key];
		}
	}

	if (typeof msg['success'] !== 'undefined') {
		if (!this[crypterSymbol]) {
			throw new Error('CrypterSymbolError');
		}
		var verified = this[crypterSymbol].verify(msg);

		if (verified) {
			if (parsedData['exec'] == 'handshake') {
				this.open = true;
			}

			if (msg['success']) {
				this.messages.get(msg.originalMsgId).callbackSuccess(msg.callbackParams);
			} else {
				this.messages.get(msg.originalMsgId).callbackError(msg.callbackParams);
			}
		} else {
			throw new Error('SignatureCheckError');
			return;
		}
		this.messages['delete'](msg.originalMsgId);
	}
}

},{"./logger":4}],8:[function(require,module,exports){
/*jshint esnext:true */
'use strict';

Object.defineProperty(exports, '__esModule', {
    value: true
});

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }

var _message = require('./message');

//import {Verifier} from './crypter';

var _basicCrypter = require('./basicCrypter');

var _messenger = require('./messenger');

var _constants = require('./constants');

var _logger = require('./logger');

var _logger2 = _interopRequireDefault(_logger);

var _singleton = _constants.singleton;
var _singletonEnforcer = _constants.singletonEnforcer;
var privates = new WeakMap();

_logger2['default'].setLevel(4); //0 - 4
var TAG = '[Stargate.js]';
/**
 * Stargate is a singleton class for hybridation of B! iframed web app
 *
 * @example
 * var instance = Stargate.initialize(configurations, publicKey, onHandshake);
 * Stargate.getInstance();
 */

var Stargate = (function () {
    function Stargate(enforcer, configurations, publicKey, onHandshake, onHandshakeFail) {
        var _this = this;

        var global = arguments[5] === undefined ? window : arguments[5];

        _classCallCheck(this, Stargate);

        if (enforcer != _constants.singletonEnforcer) throw new Error('Cannot set up a singleton with new operator');

        /** @type {Object}
          * @property {string} configurations.country - example it
          * @property {string} configurations.selector - example iphone
          * @property {string} configurations.api_selector - example zradio
          * @property {Object} configurations.hybrid_conf - the hybrid conf object
          */
        this.configurations = configurations;

        /** @type {Symbol} */
        this[_constants.handShakeSymbol] = onHandshake;

        /** @type {Symbol} */
        //this[crypterSymbol] = new Verifier(publicKey);
        this[_constants.crypterSymbol] = new _basicCrypter.BasicVerifier();

        /** @type {boolean} */
        this.open = false;

        /** @type {boolean} */
        this.initialized = true;

        /** @type {Map<string, Message>} */
        this.messages = new Map();

        /** @type {Window} */
        this.global = global;

        var readyMessage = new _message.Message('ready', onHandshake, onHandshakeFail, { 'key': 'value' });
        readyMessage.configuration = this.configurations;

        this.messages.set(readyMessage.msgId, readyMessage);

        /** @type {WeakMap} */
        privates.set(this, {
            _send: _messenger.send,
            _read: _messenger.read
        });

        //Send the ready message
        privates.get(this)._send.call(this, readyMessage);

        //if after 200ms i don't have an answer call the callback error       
        setTimeout(function () {
            if (_this.messages.has(readyMessage.msgId)) {
                onHandshakeFail();
            }
        }, 200);

        //listener will be called with Stargate and crypterSymbol
        var listener = privates.get(this)._read.bind(this, _constants.crypterSymbol);

        //Attach listener for postMessage
        this.global.addEventListener('message', listener, false);
    }

    _createClass(Stargate, [{
        key: 'inAppPurchase',

        /**
        * Purchase.
        * @param {string} productId - The id of the product to purchase
        * @param {Function} callbackSuccess - the success callback function on response
        * @param {Function} callbackError - the callbackError function on response
        * @param {string} urlForCreateUser - the url of the serverside script to create the user on our server
        */
        value: function inAppPurchase(productId, callbackSuccess, callbackError) {
            var createUserUrl = arguments[3] === undefined ? '' : arguments[3];

            var message = new _message.Message('stargate.purchase', callbackSuccess, callbackError);
            message.createUserUrl = createUserUrl;

            this.messages.set(message.msgId, message);
            privates.get(this)._send.call(this, message);
        }
    }, {
        key: 'inAppPurchaseSubscription',

        /**
        * Purchase subscription.
        * @param {Function} callbackSuccess - the callbakc success
        * @param {Function} callbackError - the callback error
        * @param {String} subscriptionUrl - the subscriptionUrl
        * @param {String} returnUrl - the returnedUrl
        */
        value: function inAppPurchaseSubscription(callbackSuccess, callbackError) {
            var subscriptionUrl = arguments[2] === undefined ? '' : arguments[2];
            var returnUrl = arguments[3] === undefined ? '' : arguments[3];

            var message = new _message.Message('stargate.purchase.subscription', callbackSuccess, callbackError);
            message.subscriptionUrl = subscriptionUrl;
            message.returnUrl = returnUrl;

            this.messages.set(message.msgId, message);
            privates.get(this)._send.call(this, message);
        }
    }, {
        key: 'inAppRestore',

        /**
        * Restore a previous purchase.
        * @param {Function} callbackSuccess - the success callback function on response
        * @param {Function} callbackError - the callbackError function on response
        * @param {string} subscriptionUrl - the url of the serverside script to create the user on our server
        * @param {string} returnUrl - returnUrl
        */
        value: function inAppRestore(callbackSuccess, callbackError) {
            var subscriptionUrl = arguments[2] === undefined ? '' : arguments[2];
            var returnUrl = arguments[3] === undefined ? '' : arguments[3];

            var message = new _message.Message('stargate.restore', callbackSuccess, callbackError);
            message.subscriptionUrl = subscriptionUrl;
            message.returnUrl = returnUrl;

            this.messages.set(message.msgId, message);
            privates.get(this)._send.call(this, message);
        }
    }, {
        key: 'facebookLogin',

        /**
        * Native Facebook login.
        * @param {string} scope - The permissions to ask
        * @param {Function} callbackSuccess - the callback success
        * @param {Function} callbackError - the callback error
        */
        value: function facebookLogin(scope, callbackSuccess, callbackError) {
            var message = new _message.Message('stargate.facebookLogin', callbackSuccess, callbackError);

            message.scope = scope;
            this.messages.set(message.msgId, message);
            privates.get(this)._send.call(this, message);
        }
    }, {
        key: 'googleLogin',

        /**
        * Native Google login.
        * @param {Function} callbackSuccess - the callback success
        * @param {Function} callbackError - the callback error
        */
        value: function googleLogin(callbackSuccess, callbackError) {
            var message = new _message.Message('stargate.googleLogin', callbackSuccess, callbackError);

            this.messages.set(message.msgId, message);
            privates.get(this)._send.call(this, message);
        }
    }, {
        key: 'openUrl',

        /**
        * It will open the url given in a browser device tab outside the web app.
        * @param {string} url - the url to open in a different window on the device
        */
        value: function openUrl(url) {
            var message = new _message.Message('system');
            message.url = url;

            this.messages.set(message.msgId, message);
            privates.get(this)._send.call(this, message);
        }
    }, {
        key: 'inIframe',

        /**
        * The inframe function description.
        * @return {boolean} Return if Stargate is running or not in iframe
        */
        value: function inIframe() {
            return this.global.self != this.global.parent;
        }
    }, {
        key: 'isOpen',

        /**
        * isOpen function
        * @return {boolean} Return true if the handshake happened
        */
        value: function isOpen() {
            return this.open;
        }
    }, {
        key: 'isInitialized',

        /**
        * isInitialized function
        * @return {boolean} Return true if the initialize method was called
        */
        value: function isInitialized() {
            return this.initialized;
        }
    }, {
        key: 'execFunctionOnApp',
        value: function execFunctionOnApp(funcName, funcArgs) {
            var message = new _message.Message(funcName);
            message.data = { 'args': funcArgs };

            this.messages.set(message.msgId, message);
            privates.get(this)._send.call(this, message);
        }
    }], [{
        key: 'initialize',

        /**
         * Build and returns a new Stargate instance.
         *
         * @param {Object} configurations - The configurations object
         * @param {String} publicKey - the publickey for the crypter object
         * @param {Function} onHandshake - the ready callback function
         * @param {Window} [global=window] - the global scope of the SDK
         * @return {Stargate} the stargate instance
         */
        value: function initialize(configurations, publicKey) {
            var onHandshake = arguments[2] === undefined ? function () {} : arguments[2];
            var onHandshakeFail = arguments[3] === undefined ? function () {} : arguments[3];
            var global = arguments[4] === undefined ? window : arguments[4];

            if (!this[_singleton]) {
                this[_singleton] = new Stargate(_constants.singletonEnforcer, configurations, publicKey, onHandshake, onHandshakeFail, global);
            } else {
                throw new Error('Cannot initialize Stargate two times');
            }
            return this[_singleton];
        }
    }, {
        key: 'getInstance',

        /**
        * Getter for the instance of the initialized stargate. if it is called before returns undefined
        * @return {Stargate} the stargate instance
        */
        value: function getInstance() {
            if (!this[_singleton]) {
                _logger2['default'].w(TAG, 'Please call initialize first: Stargate.initialize(conf, pubkey, callback)');
            }
            return this[_singleton];
        }
    }, {
        key: 'destroyInstance',

        /**
        * Destroy the current instance of the stargate setting to null the internal reference.
        */
        value: function destroyInstance() {
            this[_singleton] = null;
            _singleton = Symbol();
            _singletonEnforcer = Symbol();
        }
    }]);

    return Stargate;
})();

exports['default'] = Stargate;
module.exports = exports['default'];

},{"./basicCrypter":2,"./constants":3,"./logger":4,"./message":6,"./messenger":7}]},{},[8])(8)
});