[![Build Status](https://semaphoreci.com/api/v1/projects/4b69fe5d-6f0e-4b33-8d75-4aefe5a830c7/470216/shields_badge.svg)](https://semaphoreci.com/pasquale/es6sample)

# Stargate.js

Stargate is a singleton class for hybridation of B! iframed web appfor reference
(“[Using the ES6 transpiler Babel on Node.js](http://www.2ality.com/2015/03/babel-on-node.html)”)

# Installation
From command line run this commands:

```
#!javascript

npm install babel -g
npm install mocha -g
npm install
```

#Tests

To run tests digit the command
```
#!javascript

npm test
```

#To build
```
#!javascript

npm run build
```
#Minify
```
#!javascript

uglifyjs public/bundle.js > public/bundle.min.js
```

#Include stargate
you can find stargate in public/bundle.js.
